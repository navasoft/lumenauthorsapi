<?php

namespace App\Traits;

Use Illuminate\Http\Response;

trait ApiResponser
{
    /*
    * Build a success response
    * @param string|array $data
    * @param int $code
    * @return Illuminate\Http\Response
    */
    public function successResponse($data, $code = Response::HTTP_OK)
    {
       return response()->json(['data'=>$data], $code);
    }

    /*
    * Build an error response
    * @param string|array $message
    * @param int $code
    * @return Illuminate\Http\Response
    */
    public function errorResponse($message, $code)
    {
       return response()->json(['error'=>$message,'code'=>$code], $code);
    }
}
